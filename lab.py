from airflow import DAG, settings, models
from datetime import datetime, timedelta
import os
import json
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import GCSToBigQueryOperator

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2020, 10, 17),
    "email": ["airflow@example.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 3,
    "retry_delay": timedelta(minutes=3),
    "trigger_rule": "none_failed",
}

BQ_DS = 'SNIIM'
BQ_PROJECT = 'premium-portal-323320'
GCS_OBJECT_PATH= "" 
GCS_BUCKET = "scraping_sniim" 
SOURCE_TABLE_NAME="demo_SNNIM"

SOURCE_TABLE_NAME="df"
schema = [
    {
        'name': 'date_time',
        'type': 'STRING',
        'mode': 'NULLABLE',
    },
    {
        'name': 'Centro_distribucion',
        'type': 'STRING',
        'mode': 'NULLABLE',
    },
    {
        'name': 'valor',
        'type': 'FLOAT',
        'mode': 'NULLABLE',
    },
]

with models.DAG(
    dag_id="pass_GS_to_BQ",
    default_args=default_args,
    description=""" test """,
    catchup=False,
    is_paused_upon_creation=True,
    schedule_interval="*/10 0-4,11-23 * * *",  # Every 10 min between 6 to 23 AM Guayaquil
) as dag:

    gcs_to_bq_task = GCSToBigQueryOperator(
        task_id=f'gcs_to_bq',
        bucket=GCS_BUCKET,
        source_objects=[f'{SOURCE_TABLE_NAME}.csv'],
        destination_project_dataset_table='.'.join(
            [BQ_PROJECT, BQ_DS, SOURCE_TABLE_NAME]
        ),
        schema_fields=schema,
        create_disposition='CREATE_IF_NEEDED',
        write_disposition='WRITE_TRUNCATE',
        skip_leading_rows=1,
        allow_quoted_newlines=True,
    )